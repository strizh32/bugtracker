'use strict';

const mongoose = require('mongoose');
const user = 'root';
const password = 'root';
const url = 'ds015636.mlab.com:15636';
const dbName = 'crud-express';

mongoose.connect(`mongodb://${user}:${password}@${url}/${dbName}`);

module.exports = mongoose.connection;
'use strict';

const express = require('express');
const router = express.Router();
const User = require('../models/User');

router.get('/(:id)?', (req, res) => {
    let id = req.params.id;

    if (id) {
        User.findById(id, (err, user) => {

            if (err) {
                return res.send(err);
            }
            res.json(user);

        });
    }

    User.find((err, data) => {
        if (err) {
            return res.send(err);
        }
        res.json(data);
    });
});

router.post('/', (req, res) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let data = req.body;
    let user = new User(data);

    user.save(err => {
        if (err) {
            return res.send(err);
        }

        res.json(data);
    })
});

router.put('/:id', (req, res) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let id = req.params.id;

    User.findById(id, (err, user) => {
        if (err) {
            return res.send(err);
        }

        let data = req.body;

        for (let prop in data) {
            user[prop] = data[prop];
        }

        user.save(err => {
            if (err) {
                return res.send(err);
            }

            res.json(user);
        });
    });

});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    if (id) {
        User.findByIdAndRemove(id, (err, user) => {

            if (err) {
                return res.send(err);
            }
            return res.json(user);

        });
    } else {
        res.status(400).send('ID for removing not defined');
    }



});


module.exports = router;
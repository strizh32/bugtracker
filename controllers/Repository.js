'use strict';

const express = require('express');
const router = express.Router();
const Repository = require('../models/Repository');

router.get('/(:id)?', (req, res) => {
    let id = req.params.id;

    if (id) {
        Repository.findById(id, (err, user) => {

            if (err) {
                return res.send(err);
            }
            res.json(user);

        });
    }

    Repository.find((err, data) => {
        if (err) {
            return res.send(err);
        }
        res.json(data);
    });
});

router.post('/', (req, res) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let data = req.body;
    let user = new Repository(data);

    user.save(err => {
        if (err) {
            return res.send(err);
        }

        res.json(data);
    })
});

router.put('/:id', (req, res) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let id = req.params.id;

    Repository.findById(id, (err, user) => {
        if (err) {
            return res.send(err);
        }

        let data = req.body;

        for (let prop in data) {
            user[prop] = data[prop];
        }

        user.save(err => {
            if (err) {
                return res.send(err);
            }

            res.json(user);
        });
    });

});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    if (id) {
        Repository.findByIdAndRemove(id, (err, user) => {

            if (err) {
                return res.send(err);
            }
            return res.json(user);

        });
    } else {
        res.status(400).send('ID for removing not defined');
    }



});


module.exports = router;
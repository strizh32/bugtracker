'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BugSchema = new Schema({
    title: { type: String, default: 'Unnamed' },
    description: { type: String, default: 'blablabla' },
    userId: {type: Schema.Types.ObjectId, ref: 'User'},
    repositoryId: {type: Schema.Types.ObjectId, ref: 'Repository'}
},
    {
        timestamps: true
    });

module.exports = mongoose.model('Bug', BugSchema);
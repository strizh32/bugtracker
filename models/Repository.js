'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RepositorySchema = new Schema({
    name: { type: String, default: 'Repo' },
    userId: {type: Schema.Types.ObjectId, ref: 'User'}
},
    {
        timestamps: true
    });

module.exports = mongoose.model('Repository', RepositorySchema);
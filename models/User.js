'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: { type: String, default: 'Unnamed user' },
    password: { type: String, default: 'pwd' },
    role: { type: String, default: 'subscriber' }
},
    {
        timestamps: true
    });

module.exports = mongoose.model('User', UserSchema);



module.exports = {
    roles: [
        'admin',
        'qa',
        'developer',
        'manager',
        'subscriber'
    ]
}

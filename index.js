'use strict';

const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const mongooseConnection = require('./database/Connection');
const app = express();

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

app.set('port', 7070);
app.set('view engine', 'jade');

app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(session({
    secret: 'Barking labrador',
    store: new MongoStore({ mongooseConnection })
}));

app.use('/user', require('./controllers/User'));
app.use('/bug', require('./controllers/Bug'));
app.use('/repo', require('./controllers/Repository'));

function startServer() {
    app.listen(app.get('port'), () => {
        console.log('Express server listening on http://localhost:' + app.get('port'));
    });
};

mongooseConnection.on('error', console.error.bind(console, 'connection error:'));
mongooseConnection.once('open', startServer);